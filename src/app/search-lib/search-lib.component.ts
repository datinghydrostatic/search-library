import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { SearchLibService } from './search-lib.service';

@Component({
  selector: 'app-search-lib',
  templateUrl: './search-lib.component.html',
  providers: [SearchLibService],
  styleUrls: ['./search-lib.component.scss']
})
export class SearchLibComponent implements OnInit {

  theForm: FormGroup;
  options: any[] = [];
  filteredOptions: Observable<any[]>;
  isLoading = false;
  @Output() searchResult: EventEmitter<any> = new EventEmitter();

  constructor(public searchLibService: SearchLibService) {
  }

  ngOnInit() {

    this.theForm = new FormGroup({
      autoControl: new FormControl(null, Validators.required),
      dateControl: new FormControl(new Date(), Validators.required)
    });
    this.searchLibService.getSearchData()
      .subscribe(res => {
        this.options = res.data;
      });
    this.filteredOptions = this.theForm.controls.autoControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.employee_name),
        map(name => name ? this._filter(name) : this.options.slice())
      );
  }

  onSearchClick() {
    const id = this.theForm.controls.autoControl.value.id;
    this.isLoading = true;
    this.searchLibService.getSingleData(id)
      .subscribe(res => {
        this.searchResult.emit(res.data);
        this.isLoading = false;
      });

  }

  displayFn(user: any): string {
    return user && user.employee_name ? user.employee_name : '';
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(option => option.employee_name.toLowerCase().indexOf(filterValue) === 0);
  }

}
