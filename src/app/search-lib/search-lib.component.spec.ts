import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { SearchLibComponent } from './search-lib.component';
import { SearchLibModule } from './search-lib.module';

describe('SearchLibComponent', () => {
  let component: SearchLibComponent;
  let fixture: ComponentFixture<SearchLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SearchLibModule,
      ],
      declarations: [SearchLibComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
