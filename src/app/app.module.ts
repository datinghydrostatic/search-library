import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { SearchLibModule } from './search-lib/search-lib.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SearchLibModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
