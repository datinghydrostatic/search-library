import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'search-lib';
  searchedData = 'There is no search result';

  onDataOut(ev: any) {
    this.searchedData = ev;
  }
}
